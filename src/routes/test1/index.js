import React from 'react';
import Layout from '../../components/Layout';
import Test1 from './Test1';
import fetch from '../../core/fetch';

export default {

  path: '/test1',

  async action() {
    const resp = await fetch('/test1', {
      method: 'post',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      credentials: 'include',
    });

    const data = await resp.json();
    if (!data) throw new Error('Failed to load the test1.');
    return {
      title: 'Test1',
      component: <Layout><Test1 items={data} /></Layout>,
    };
  },

};
