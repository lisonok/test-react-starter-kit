import React, { PropTypes } from 'react';

class Test1 extends React.Component {
  static propTypes = {
    items: PropTypes.arrayOf(PropTypes.shape({
      price: PropTypes.number.isRequired,
      title: PropTypes.string.isRequired
    })).isRequired,
  };

  render() {
    return (
      <div>
        <div>
          <ul>
            {this.props.items.map((item, index) => (
              <li key={index}>
                {item.title} - {item.price}$
              </li>
            ))}
          </ul>
        </div>
      </div>
    );
  }
}

export default Test1;
