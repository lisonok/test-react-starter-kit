import React from 'react';
import Layout from '../../components/Layout';
import Test2 from './Test2';
import fetch from '../../core/fetch';

export default {

  path: '/test2',

  async action() {
    const resp = await fetch('/test2', {
      method: 'post',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      credentials: 'include',
    });

    const data = await resp.json();
    if (!data) throw new Error('Failed to load the test2.');
    return {
      title: 'Test2',
      component: <Layout><Test2 items={data} /></Layout>,
    };
  },

};
