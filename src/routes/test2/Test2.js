import React, { PropTypes } from 'react';

class Test2 extends React.Component {
  static propTypes = {
    items: PropTypes.arrayOf(PropTypes.shape({
      name: PropTypes.string.isRequired,
      descr: PropTypes.string.isRequired
    })).isRequired,
  };

  render() {
    return (
      <div>
        <div>
          <h1>Reviews</h1>
          <ul>
            {this.props.items.map((item, index) => (
              <li key={index}>
                <strong>{item.name}</strong>: <i>{item.descr}</i>
              </li>
            ))}
          </ul>
        </div>
      </div>
    );
  }
}

export default Test2;
